#!/usr/bin/env python3.6
import configparser
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
import os.path
import time
import sys
import smtplib
import shutil

from ftplib import FTP

def send_email():
    """
    Método para enviar correo electronico
    """
    # Open a plain text file for reading.  For this example, assume that
    # the text file contains only ASCII characters.
    # Create a text/plain message
    msg = MIMEMultipart()

    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = EMAIL_SUBJECT
    msg['From'] = EMAIL_FROM
    msg['To'] = EMAIL_TO
    body = MIMEText(EMAIL_BODY.format(PATHS_TO_BACKUP), 'html')
    msg.attach(body)

    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    smtp = smtplib.SMTP(SMTP_HOST)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    try:
        smtp.login(SMTP_USER, SMTP_PASSWORD)
        smtp.sendmail(msg['From'], msg['To'], msg.as_string())
        smtp.quit()
    except Exception as e:
        print(e)


# Get Configurations from file mysql client of this script
CONFIG = configparser.ConfigParser()
CONFIG_PATH = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE_PATH = "%s/backups.conf" % CONFIG_PATH
print('path =', CONFIG_PATH)

CONFIG_FILE_PATH = os.path.isfile(CONFIG_FILE_PATH) and CONFIG_FILE_PATH or sys.argv[1]

if os.path.isfile(CONFIG_FILE_PATH):
    CONFIG.read(CONFIG_FILE_PATH)

    PATHS_TO_BACKUP = CONFIG.get('folders', 'paths')

    LOCAL_PATH_BACKUPS = CONFIG.get('backups', 'path_local')

    USERNAME_FTP = CONFIG.get('ftp', 'user_ftp')
    PASSWORD_FTP = CONFIG.get('ftp', 'password_ftp')
    HOST_FTP = CONFIG.get('ftp', 'host_ftp')
    REMOTE_PATH_BACKUPS = CONFIG.get('ftp', 'path_remote')
    MAX_SIZE_ROLLBACKUPS = CONFIG.get('ftp', 'max_size_rollbackups')

    SMTP_HOST = CONFIG.get('email', 'smtp_host')
    SMTP_USER = CONFIG.get('email', 'smtp_user')
    SMTP_PASSWORD = CONFIG.get('email', 'smtp_password')
    EMAIL_FROM = CONFIG.get('email', 'from')
    EMAIL_TO = CONFIG.get('email', 'to')
    EMAIL_SUBJECT = CONFIG.get('email', 'subject')
    EMAIL_BODY = CONFIG.get('email', 'body')

    FILES_STAMP = time.strftime('-%Y-%m-%d')

    print(time.strftime('%Y-%m-%d %H:%M:%S'))

    if not os.path.exists(LOCAL_PATH_BACKUPS):
        os.makedirs(LOCAL_PATH_BACKUPS)

    FOLDERS = str(PATHS_TO_BACKUP).split(';')

    for folder in FOLDERS:
        folder = folder.strip()

        if not os.path.exists(folder):
            print("Folder '{0}' doesn't exist".format(folder))
            continue

        folder_name = os.path.basename(folder)

        filename = "%s%s" % (folder_name, FILES_STAMP)
        backupFileName = "%s.zip" % filename
        backupFile = "%s/%s" % (LOCAL_PATH_BACKUPS, filename)
        backupFileZip = "%s/%s" % (LOCAL_PATH_BACKUPS, backupFileName)

        if os.path.exists(backupFileZip):
            os.remove(backupFileZip)

        shutil.make_archive(backupFile, 'zip', folder)

        if not os.path.exists(backupFileZip):
            print("Can't create zip backup of this folder '{0}' ({1})".format(folder, backupFile))
            continue

        print("%s backup folder is done !" % folder)

        ftp = FTP(HOST_FTP)
        ftp.login(USERNAME_FTP, PASSWORD_FTP)

        try:
            ftp.cwd(REMOTE_PATH_BACKUPS)
        except Exception as e:
            ftp.mkd(REMOTE_PATH_BACKUPS)
            ftp.cwd(REMOTE_PATH_BACKUPS)
            print(e)

        fileStream = open(backupFileZip, 'rb')

        ftp.cwd(REMOTE_PATH_BACKUPS)

        print(REMOTE_PATH_BACKUPS)

        if not folder_name in ftp.nlst():
            ftp.mkd(folder_name)

        ftp.cwd(folder_name)
        ftp.storbinary('STOR %s' % backupFileName, fileStream)
        print("%s is uploaded !" % backupFileName)
        os.remove(backupFileZip)
        print('Remove local backup "%s"' % backupFileZip)

        # Obtener listado de archivos existentes de la base de datos actual
        current_backups = ftp.nlst()
        current_backups.sort(reverse=True)

        count_backups = 1
        max_backups = int(MAX_SIZE_ROLLBACKUPS)
        count_curr_backups = current_backups.__len__()

        # Mantener solo el numero de copias de respaldo del valor de max_size_rollbackups
        if count_curr_backups > max_backups:
            for backup in current_backups:
                # Borrar el restante de los respaldos
                if count_backups > max_backups:
                    ftp.delete(backup)
                count_backups += 1

        ftp.quit()
        ftp.close()

    print("all folders backups are done !")
    print(time.strftime('%Y-%m-%d %H:%M:%S'))

    send_email()
    print("the email is been successful sent")
else:
    print('Not found Backups.conf file')
